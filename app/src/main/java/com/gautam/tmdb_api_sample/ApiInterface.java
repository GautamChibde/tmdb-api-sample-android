package com.gautam.tmdb_api_sample;

import com.gautam.tmdb_api_sample.model.detail.MovieDetail;
import com.gautam.tmdb_api_sample.model.detail.MovieImages;
import com.gautam.tmdb_api_sample.model.UpcomingMovies;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by gautam on 6/11/16.
 */
public interface ApiInterface {
    @GET("movie/upcoming")
    Observable<UpcomingMovies> getUpcomingMovies(
            @Query("api_key") String key,
            @Query("page") int page);

    @GET("movie/{id}")
    Call<MovieDetail> getMovieDetail(
            @Path("id") long id,
            @Query("api_key") String key);

    @GET("movie/{id}/images")
    Call<MovieImages> getMovieImages(
            @Path("id") long id,
            @Query("api_key") String key);
}
