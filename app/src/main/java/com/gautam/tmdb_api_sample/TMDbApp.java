package com.gautam.tmdb_api_sample;

import com.gc.comman.App;
import com.squareup.leakcanary.LeakCanary;

/**
 * Created by gautam on 12/4/17.
 */
public class TMDbApp extends App {
    public static final String API_URL = "https://api.themoviedb.org/3/";
    public static final String API_KEY = "b7cd3340a794e5a2f35e3abb820b497f";
    public static final String ENDPINT_IMAGE = "http://image.tmdb.org/t/p/w500/";

    @Override
    public void onCreate() {
        super.onCreate();
        setBaseUrl(API_URL);
        if (LeakCanary.isInAnalyzerProcess(this)) {
            // This process is dedicated to LeakCanary for heap analysis.
            // You should not init your app in this process.
            return;
        }
        LeakCanary.install(this);
    }
}
