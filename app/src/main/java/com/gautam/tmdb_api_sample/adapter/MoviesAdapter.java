package com.gautam.tmdb_api_sample.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.gautam.tmdb_api_sample.R;
import com.gautam.tmdb_api_sample.TMDbApp;
import com.gautam.tmdb_api_sample.ui.MovieDetailActivity;
import com.gautam.tmdb_api_sample.model.Movies;
import com.gc.comman.util.Constant;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by gautam on 12/4/17.
 */
public class MoviesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Movies> movies;
    private Context context;

    public MoviesAdapter(Context context, List<Movies> movies) {
        this.context = context;
        this.movies = movies;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == 1) {
            return new MoviesHolder(LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_movies, parent, false));
        } else {
            return new ProgressViewHolder(LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_progress, parent, false));
        }
    }

    @Override
    public int getItemViewType(int position) {
        return movies.get(position) != null ? 1 : 0;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof MoviesHolder) {
            onBindMovieViewHolder((MoviesHolder) holder, movies.get(position));
        } else {
            ((ProgressViewHolder) holder).progressBar.setIndeterminate(true);
        }
    }

    public void onBindMovieViewHolder(MoviesHolder holder, final Movies movie) {
        holder.name.setText(movie.getTitle());
        holder.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(
                        context,
                        MovieDetailActivity.class)
                        .putExtra(Constant.KEY_USER_ID, movie.getId())
                        .putExtra(Constant.TITLE, movie.getTitle()));
            }
        });
        Picasso.with(context)
                .load(TMDbApp.ENDPINT_IMAGE + movie.getPoster_path())
                .placeholder(R.drawable.loading_placeholder)
                .error(R.drawable.poster_not_found)
                .into(holder.poster);
        holder.releaseDate.setText(movie.getRelease_date());
        if (movie.isAdult()) {
            holder.rated.setText("A");
        } else {
            holder.rated.setText("U/A");
        }
        holder.overview.setText(movie.getOverview());
        holder.rating.setText(String.valueOf(movie.getVote_average() + "/" + 10));
    }

    @Override
    public int getItemCount() {
        return movies.size();
    }

    public void addItems(List<Movies> results) {
        for (Movies movie : results) {
            this.movies.add(movie);
            notifyItemInserted(movies.size());
        }
    }

    public void addItem(Movies movie) {
        movies.add(movie);
        notifyItemInserted(movies.size());
    }

    public void removeLastItem() {
        movies.remove(movies.size() - 1);
        notifyItemRemoved(movies.size());
    }

    public static class ProgressViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public ProgressViewHolder(View v) {
            super(v);
            progressBar = (ProgressBar) v.findViewById(R.id.progressBar);
        }
    }

    public static class MoviesHolder extends RecyclerView.ViewHolder {
        TextView name, releaseDate, rated, overview, rating;
        CardView container;
        ImageView poster;

        public MoviesHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.tv_name);
            container = (CardView) itemView.findViewById(R.id.cv_container);
            poster = (ImageView) itemView.findViewById(R.id.iv_poster);
            releaseDate = (TextView) itemView.findViewById(R.id.tv_release_data);
            rated = (TextView) itemView.findViewById(R.id.tv_rated);
            overview = (TextView) itemView.findViewById(R.id.tv_overview);
            rating = (TextView) itemView.findViewById(R.id.tv_rating);
        }
    }
}
