package com.gautam.tmdb_api_sample.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.List;

/**
 * Created by gautam on 12/4/17.
 */
public class UpcomingMovies implements Parcelable {
    private int page;
    private List<Movies> results;

    public UpcomingMovies() {
    }

    protected UpcomingMovies(Parcel in) {
        page = in.readInt();
        results = in.createTypedArrayList(Movies.CREATOR);
    }

    public static final Creator<UpcomingMovies> CREATOR = new Creator<UpcomingMovies>() {
        @Override
        public UpcomingMovies createFromParcel(Parcel in) {
            return new UpcomingMovies(in);
        }

        @Override
        public UpcomingMovies[] newArray(int size) {
            return new UpcomingMovies[size];
        }
    };

    @Override
    public String toString() {
        return "UpcomingMovies{" +
                "page=" + page +
                ", results=" + results +
                '}';
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public List<Movies> getResults() {
        return results;
    }

    public void setResults(List<Movies> results) {
        this.results = results;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(page);
        dest.writeTypedList(results);
    }
}
