package com.gautam.tmdb_api_sample.model.detail;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by gautam on 12/4/17.
 */
public class BackDrop implements Parcelable {
    private String file_path;

    public BackDrop() {
    }

    @Override
    public String toString() {
        return "BackDrop{" +
                "file_path='" + file_path + '\'' +
                '}';
    }

    public String getFile_path() {
        return file_path;
    }

    public void setFile_path(String file_path) {
        this.file_path = file_path;
    }

    protected BackDrop(Parcel in) {
        file_path = in.readString();
    }

    public static final Creator<BackDrop> CREATOR = new Creator<BackDrop>() {
        @Override
        public BackDrop createFromParcel(Parcel in) {
            return new BackDrop(in);
        }

        @Override
        public BackDrop[] newArray(int size) {
            return new BackDrop[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(file_path);
    }
}
