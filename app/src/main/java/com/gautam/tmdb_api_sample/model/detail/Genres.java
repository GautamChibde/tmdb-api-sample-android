package com.gautam.tmdb_api_sample.model.detail;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by gautam on 12/4/17.
 */
public class Genres implements Parcelable {
    private String name;

    public Genres() {
    }

    protected Genres(Parcel in) {
        name = in.readString();
    }

    public static final Creator<Genres> CREATOR = new Creator<Genres>() {
        @Override
        public Genres createFromParcel(Parcel in) {
            return new Genres(in);
        }

        @Override
        public Genres[] newArray(int size) {
            return new Genres[size];
        }
    };

    @Override
    public String toString() {
        return "Genres{" +
                "name='" + name + '\'' +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
    }
}
