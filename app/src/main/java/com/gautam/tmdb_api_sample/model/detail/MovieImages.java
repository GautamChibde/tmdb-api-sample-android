package com.gautam.tmdb_api_sample.model.detail;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by gautam on 12/4/17.
 */
public class MovieImages implements Parcelable {
    private long id;
    private List<BackDrop> backdrops;

    public MovieImages() {
    }

    protected MovieImages(Parcel in) {
        id = in.readLong();
        backdrops = in.createTypedArrayList(BackDrop.CREATOR);
    }

    public static final Creator<MovieImages> CREATOR = new Creator<MovieImages>() {
        @Override
        public MovieImages createFromParcel(Parcel in) {
            return new MovieImages(in);
        }

        @Override
        public MovieImages[] newArray(int size) {
            return new MovieImages[size];
        }
    };

    @Override
    public String toString() {
        return "MovieImages{" +
                "id=" + id +
                ", backdrops=" + backdrops +
                '}';
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public List<BackDrop> getBackdrops() {
        return backdrops;
    }

    public void setBackdrops(List<BackDrop> backdrops) {
        this.backdrops = backdrops;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeTypedList(backdrops);
    }
}
