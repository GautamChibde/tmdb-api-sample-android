package com.gautam.tmdb_api_sample.presenter;

import com.gautam.tmdb_api_sample.ApiInterface;

/**
 * Created by gautam on 26/4/17.
 */
abstract public class BasePresenter {

    private ApiInterface apiInterface;

    abstract public void detach();

    public BasePresenter(ApiInterface apiInterface) {
        this.apiInterface = apiInterface;
    }

    public ApiInterface getApiInterface() {
        return apiInterface;
    }
}
