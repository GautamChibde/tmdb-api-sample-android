package com.gautam.tmdb_api_sample.presenter;

import com.gautam.tmdb_api_sample.model.UpcomingMovies;

/**
 * Created by gautam on 26/4/17.
 */
public interface MainMvpView extends BaseMvpView {
    void onLoadNewMovies(UpcomingMovies movies);
}
