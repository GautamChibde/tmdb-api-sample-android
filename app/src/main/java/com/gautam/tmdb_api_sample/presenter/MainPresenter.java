package com.gautam.tmdb_api_sample.presenter;

import com.gautam.tmdb_api_sample.ApiInterface;
import com.gautam.tmdb_api_sample.TMDbApp;
import com.gautam.tmdb_api_sample.model.UpcomingMovies;

import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by gautam on 26/4/17.
 */
public class MainPresenter extends BasePresenter {
    private MainMvpView mvpView;
    private Subscription mSubscription;

    public MainPresenter(MainMvpView mvpView, ApiInterface apiInterface) {
        super(apiInterface);
        this.mvpView = mvpView;
    }

    public void getNextPage(int page_number) {
        mSubscription = getApiInterface().getUpcomingMovies(TMDbApp.API_KEY, page_number)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<UpcomingMovies>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        mvpView.onError();
                    }

                    @Override
                    public void onNext(UpcomingMovies movies) {
                        mvpView.onLoadNewMovies(movies);
                    }
                });
    }

    @Override
    public void detach() {
        if (mSubscription != null) mSubscription.unsubscribe();
    }
}
