package com.gautam.tmdb_api_sample.presenter;

import com.gautam.tmdb_api_sample.model.UpcomingMovies;

/**
 * Created by gautam on 26/4/17.
 */
public interface SplashScreenMvpView extends BaseMvpView {
    void onLoadMovies(UpcomingMovies movies);
}
