package com.gautam.tmdb_api_sample.presenter;

import com.gautam.tmdb_api_sample.ApiInterface;
import com.gautam.tmdb_api_sample.TMDbApp;
import com.gautam.tmdb_api_sample.model.UpcomingMovies;

import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by gautam on 26/4/17.
 */
public class SplashScreenPresenter extends BasePresenter {
    private SplashScreenMvpView listener;
    private Subscription mSubscription;

    public SplashScreenPresenter(SplashScreenMvpView listener, ApiInterface apiInterface) {
        super(apiInterface);
        this.listener = listener;
    }

    public void getMovies() {
        mSubscription = getApiInterface().getUpcomingMovies(TMDbApp.API_KEY, 1)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<UpcomingMovies>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        listener.onError();
                    }

                    @Override
                    public void onNext(UpcomingMovies movies) {
                        listener.onLoadMovies(movies);
                    }
                });
    }

    @Override
    public void detach() {
        if (mSubscription != null) mSubscription.unsubscribe();
    }
}
