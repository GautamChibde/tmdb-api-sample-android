package com.gautam.tmdb_api_sample.ui;

import android.os.Bundle;
import android.widget.TextView;

import com.gautam.tmdb_api_sample.R;
import com.gc.comman.ui.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class InfoActivity extends BaseActivity {

    @BindView(R.id.tv_dev_info) TextView devInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        setTitle("Information");
        devInfo.setText("Developed by \nGautam Chibde");
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_info;
    }

    @Override
    protected int getToolbarId() {
        return R.id.toolbar;
    }

    @Override
    protected int getTitleId() {
        return R.id.tvTitle;
    }
}
