package com.gautam.tmdb_api_sample.ui;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;

import com.gautam.tmdb_api_sample.ApiInterface;
import com.gautam.tmdb_api_sample.R;
import com.gautam.tmdb_api_sample.adapter.MoviesAdapter;
import com.gautam.tmdb_api_sample.model.UpcomingMovies;
import com.gautam.tmdb_api_sample.presenter.MainMvpView;
import com.gautam.tmdb_api_sample.presenter.MainPresenter;
import com.gc.comman.App;
import com.gc.comman.ui.BaseActivity;
import com.gc.comman.ui.EndlessRecyclerOnScrollListener;
import com.gc.comman.util.Constant;

import butterknife.ButterKnife;

public class MainActivity extends BaseActivity implements MainMvpView {
    private MoviesAdapter moviesAdapter;
    private MainPresenter mainPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.rv_movies);
        UpcomingMovies upcomingMovies = getIntent().getParcelableExtra(Constant.UPCOMING_MOVIES);
        setTitle("Upcoming Movies");
        mainPresenter = new MainPresenter(this, App.getClient().create(ApiInterface.class));
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        assert recyclerView != null;
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.addOnScrollListener(new EndlessRecyclerOnScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                moviesAdapter.addItem(null);
                mainPresenter.getNextPage(current_page);
            }
        });
        moviesAdapter = new MoviesAdapter(this, upcomingMovies.getResults());
        recyclerView.setAdapter(moviesAdapter);
    }

    @Override
    public void onLoadNewMovies(UpcomingMovies movies) {
        moviesAdapter.removeLastItem();
        moviesAdapter.addItems(movies.getResults());
    }

    @Override
    public void onError() {
        moviesAdapter.removeLastItem();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_info) {
            startNewActivity(InfoActivity.class);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    protected int getToolbarId() {
        return R.id.toolbar;
    }

    @Override
    protected int getTitleId() {
        return R.id.tvTitle;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mainPresenter.detach();
    }

    @Override
    protected boolean displayHomeButton() {
        return false;
    }
}
