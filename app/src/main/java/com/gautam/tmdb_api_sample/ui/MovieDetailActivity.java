package com.gautam.tmdb_api_sample.ui;

import android.os.Bundle;
import android.view.View;
import android.widget.RatingBar;
import android.widget.TextView;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.gautam.tmdb_api_sample.ApiInterface;
import com.gautam.tmdb_api_sample.R;
import com.gautam.tmdb_api_sample.TMDbApp;
import com.gautam.tmdb_api_sample.model.detail.BackDrop;
import com.gautam.tmdb_api_sample.model.detail.MovieDetail;
import com.gautam.tmdb_api_sample.model.detail.MovieImages;
import com.gc.comman.App;
import com.gc.comman.infra.Resource;
import com.gc.comman.ui.BaseActivity;
import com.gc.comman.util.Constant;

import java.util.List;

import retrofit2.Call;

public class MovieDetailActivity extends BaseActivity {
    private long id;
    private SliderLayout mSlider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        id = getIntent().getLongExtra(Constant.KEY_USER_ID, -1);
        mSlider = (SliderLayout) findViewById(R.id.slider);
        ApiInterface apiService = App.getClient().create(ApiInterface.class);
        setTitle(getIntent().getStringExtra(Constant.TITLE));
        Call<MovieDetail> productCategoryCall = apiService.getMovieDetail(
                id,
                TMDbApp.API_KEY);
        Resource.enquireClone(productCategoryCall, 200);
        progress.show();
    }

    @SuppressWarnings("unused")
    public void onEvent(Resource.HttpRequestComplete httpRequestComplete) {
        if (httpRequestComplete.getRequestId() == 200) {
            if (httpRequestComplete.isSuccessful()) {
                MovieDetail movieDetail = (MovieDetail) httpRequestComplete.getResponse();
                setDetails(movieDetail);
            }
            getMovieImages();
        }
        if (httpRequestComplete.getRequestId() == 300) {
            progress.dismiss();
            if (httpRequestComplete.isSuccessful()) {
                MovieImages images = (MovieImages) httpRequestComplete.getResponse();
                setSlider(images.getBackdrops());
            }
        }
    }

    private void setDetails(MovieDetail movieDetail) {
        ((TextView) findViewById(R.id.tv_movie_title)).setText(movieDetail.getTitle());
        ((TextView) findViewById(R.id.tv_movie_overview)).setText(movieDetail.getOverview());
        RatingBar ratingBar = (RatingBar) findViewById(R.id.ratingBar);
        ratingBar.setVisibility(View.VISIBLE);
        ratingBar.setRating(movieDetail.getVote_average() / 2);
    }

    private void setSlider(List<BackDrop> backdrops) {
        for (BackDrop backDrop : backdrops.size() >= 5 ? backdrops.subList(0, 4) : backdrops) {
            TextSliderView textSliderView = new TextSliderView(this);
            textSliderView
                    .description("")
                    .image(TMDbApp.ENDPINT_IMAGE + backDrop.getFile_path())
                    .setScaleType(BaseSliderView.ScaleType.Fit);
            textSliderView.bundle(new Bundle());
            textSliderView.getBundle()
                    .putString("extra", backDrop.getFile_path());

            mSlider.addSlider(textSliderView);
        }
        mSlider.setPresetTransformer(SliderLayout.Transformer.Accordion);
        mSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        mSlider.setCustomAnimation(new DescriptionAnimation());
        mSlider.setDuration(10000);
    }

    private void getMovieImages() {
        ApiInterface apiService = App.getClient().create(ApiInterface.class);
        Call<MovieImages> movieDetailCall = apiService.getMovieImages(
                id,
                TMDbApp.API_KEY);
        Resource.enquireClone(movieDetailCall, 300);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_movie_detail;
    }

    @Override
    protected int getToolbarId() {
        return R.id.toolbar;
    }

    @Override
    protected int getTitleId() {
        return R.id.tvTitle;
    }
}
