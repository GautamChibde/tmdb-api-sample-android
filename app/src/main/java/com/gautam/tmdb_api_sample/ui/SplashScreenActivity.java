package com.gautam.tmdb_api_sample.ui;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;

import com.gautam.tmdb_api_sample.ApiInterface;
import com.gautam.tmdb_api_sample.R;
import com.gautam.tmdb_api_sample.model.UpcomingMovies;
import com.gautam.tmdb_api_sample.presenter.SplashScreenMvpView;
import com.gautam.tmdb_api_sample.presenter.SplashScreenPresenter;
import com.gc.comman.App;
import com.gc.comman.ui.BaseActivity;
import com.gc.comman.util.Constant;
import com.gc.comman.util.PlatformUtil;

public class SplashScreenActivity extends BaseActivity implements SplashScreenMvpView {

    private SplashScreenPresenter splashScreenPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        splashScreenPresenter = new SplashScreenPresenter(
                this,
                App.getClient().create(ApiInterface.class));
        postServer();
    }

    private void postServer() {
        splashScreenPresenter.getMovies();
    }

    private void networkErrorDialog(String message) {
        new AlertDialog.Builder(this)
                .setTitle("network error")
                .setMessage(message)
                .setPositiveButton(R.string.retry, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        postServer();
                        dialog.dismiss();
                    }
                })
                .setIcon(R.drawable.ic_close_white_24dp)
                .setIconAttribute(android.R.attr.alertDialogIcon)
                .show();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_splash_screen;
    }

    @Override
    public void onLoadMovies(UpcomingMovies movies) {
        startNewActivity(MainActivity.class,
                PlatformUtil.bundleParcelable(Constant.UPCOMING_MOVIES, movies)
        );
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        splashScreenPresenter.detach();
    }

    @Override
    public void onError() {
        networkErrorDialog("Problem with your network");
    }
}
