package com.gc.comman.util;

import java.text.SimpleDateFormat;
import java.util.Locale;

public interface Constant {
    String DATE_PATTERN = "yyyy-MM-dd HH:mm:ss.SSSSSS";
    SimpleDateFormat timeFormatter =
            new SimpleDateFormat("HH:mm", Locale.getDefault());
    SimpleDateFormat defaultDateFormatter =
            new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());

    String MODEL = "MODEL";

    String KEY_USER_TOKEN = "KEY_USER_TOKEN";
    String KEY_CLIENT_TOKEN = "KEY_CLIENT_TOKEN";
    String KEY_USER_ROL = "KEY_USER_ROL";
    String KEY_USER_ID = "KEY_USER_ID";
    String LOGGED_IN_USER = "logged in user";
    String BASIC_AUTH = "auth";
    String TITLE = "title";
    String CONTEXT_SIGNUP = "/api/signup";
    String CONTEXT_TOKEN = "/api/token";
    String REPO = "repo";
    String GRANT_TYPE_PASSWORD = "password";
    String GRANT_TYPE_CLIENT_CREDENTIALS = "client_credentials";
    String USER = "user";
    String FOR_RESULT = "FOR_RESULT";
    String REQUEST_CODE = "REQUEST_CODE";
    String GIT_AUTH = "git_auth";
    String REPOSITORIES = "repos";
    String COMMITS = "commits";
    String COLLABORATORS = "collaborators";
    String BACKDROPS = "backdrops";

    String PRODUCT_CATEGORY = "category";
    String PRODUCT = "product";
    String POSITION = "position";
    String UPCOMING_MOVIES = "upcoming_movies";

    int UPCOMING_MOVIES_REQUEST_CODE = 101;
    int AUTH_REQUEST_CODE = 102;
    int REPOSITORIES_REQUEST_CODE = 103;
    int FOLLOWER_REQUEST_CODE = 105;
    int FOLLOWING_REQUEST_CODE = 106;
    int COMMIT_REQUEST_CODE = 107;
    int COLLABORATORS_REQUEST_CODE = 108;
}
